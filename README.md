# zpool-alerts.py
`zpool-alerts.py` is a Python script for monitoring the health of ZFS pools.
 
It uses the [PyPushover](https://gitlab.com/alingo/pypushover/) module for sending [Pushover](https://pushover.net/) alert messages.

For `zpool` command information, please see the FreeBSD [zpool(8)](https://www.freebsd.org/cgi/man.cgi?zpool(8)) manual page.

For Pushover API details, please see the [Official Pushover API Documentation](https://pushover.net/api).

## Installation
`zpool-alerts.py` requires Python 3.7+ and the [PyPushover](https://gitlab.com/alingo/pypushover/) module. `zpool-alerts.py` must be run as `root`, and cannot be run in a jail.

Copy the `settings.ini.example` file to `/root/.config/zpool-alerts/settings.ini` and modify it as explained below.

## Health Checks
`check_pool_capacity` - Checks the used capacity percentage of ZFS pools, and alerts at various thresholds.

`check_pool_health` - Checks the pool health state of ZFS pools, and alerts for any state other than `ONLINE`.

## Configuration Options
`zpool-alerts.py` is configured though the `/root/.config/zpool-alerts/settings.ini` file.

### global Section
`IgnorePools` -- A comma-separated list of ZFS pool names that should be ignored by this script.

### pushover Section
`ApplicationToken` - The Pushover Application Token for the application to send the alerts.

`UserKey` - Your Pushover User Key seen on the Pushover dashboard.

`NormalPrioritySound` - The name of the [Pushover sound](https://pushover.net/api#sounds) to play for a normal priority alert.

`HighPrioritySound` - The name of the [Pushover sound](https://pushover.net/api#sounds) to play for a high priority alert.

`EmergencyPrioritySound` - The name of the [Pushover sound](https://pushover.net/api#sounds) to play for an emergency priority alert.

`EmergencyAlertRetry` - How many seconds between alert re-deliveries of unacknowledged emergency priority alerts.

`EmergencyAlertExpire` - How many seconds without alert acknowledgment to attempt to re-deliver emergency priority alerts.

### check_pool_capacity Section
`IgnorePools` - A comma-separated list of ZFS pool names that should be ignored by the `check_pool_capacity` health check.

`NormalAlertThreshold` - The used capacity percentage of a ZFS pool that triggers a normal priority alert.

`HighAlertThreshold` - The used capacity percentage of a ZFS pool that triggers a high priority alert.

`EmergencyAlertThreshold` - The used capacity percentage of a ZFS pool that triggers an emergency priority alert.

### check_pool_health Section
`IgnorePools`  - A comma-separated list of ZFS pool names that should be ignored by the `check_pool_health` health check.

