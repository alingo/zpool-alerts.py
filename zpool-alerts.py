#!/usr/local/bin/python3.7
"""
zpool-alerts.py is a script to check ZFS pool health and fire off Pushover alerts if issues are detected.
"""
import datetime
import configparser
import json
import os
import socket
import subprocess
import sys
from PyPushover import PushoverMessage, PushoverError


def load_conf_file(conf_file):
    """
    Load the script's configuration file.
    :param conf_file: The file to read configuration from.
    :return: Config parser configuration data
    """
    parser = configparser.ConfigParser()
    try:
        parser.read(conf_file)
    except FileNotFoundError:
        # The conf file doesn't exist -- bail!
        print(f"Fatal error loading configuration file '{ conf_file }'!")
        sys.exit(2)
    return parser


def load_state_file(state_file):
    """
    Load the script's state file.
    :param state_file: The file to read from to get the script's state information.
    :returns: Dictionary of state information
    """
    try:
        with open(state_file) as f:
            data = json.load(f)
    except FileNotFoundError:
        # The state file doesn't exist -- start over
        print(f"Error loading state file '{ state_file }'! Continuing...")
        data = {}
    return data


def save_state_file(state_file):
    """
    Write the script's state file
    :param state_file: The file to write to with the script's state information.
    """
    # Make sure the path exists and make it if not
    if not os.path.exists(os.path.dirname(state_file)):
        os.makedirs(os.path.dirname(state_file))

    # Try to write the file
    try:
        with open(state_file, 'w') as f:
            f.write(json.dumps(state))
    except FileNotFoundError:
        print(f"Fatal error writing state file '{state_file}'!")
        sys.exit(2)


def send_alert(message, title, category, priority=0):
    """
    Sends a Pushover alert message if a similar alert is not outstanding.

    An alert is considered 'similar' if it shares the same category and title.

    :param message: The message text for the alert.
    :param title: The title text for the alert.
    :param category: The category of the alert. (What health check function is alerting)
    :param priority: The Pushover priority of the alert.
    """
    # Make sure our state data structure is setup
    if 'active_alerts' not in list(state):
        state['active_alerts'] = {}
    if category not in state['active_alerts']:
        state['active_alerts'][category] = {}

    # Make sure we've not already sent this alert
    if title in list(state['active_alerts'][category]):
        print(f"Already sent '{ title }' in '{ category }', not sending alert...")
        return

    # Build the alert
    alert = PushoverMessage(conf['pushover']['ApplicationToken'], conf['pushover']['UserKey'], message,
                            title=title, priority=priority)

    # Configure the alert based on priority
    if priority == 2:
        alert.set_sound(conf['pushover']['EmergencyPrioritySound'])
        alert.set_retry(int(conf['pushover']['EmergencyAlertRetry']))
        alert.set_expire(int(conf['pushover']['EmergencyAlertRetry']))
    if priority == 1:
        alert.set_sound(conf['pushover']['HighPrioritySound'])
    else:
        alert.set_sound(conf['pushover']['NormalPrioritySound'])

    # Send the alert.
    try:
        alert.send()
    except PushoverError:
        print(f"Error sending Pushover alert '{ message}'!")
        return

    # Store the alert in the state structure
    state['active_alerts'][category][title] = {'time': datetime.datetime.timestamp(datetime.datetime.now()),
                                               'priority': priority, 'message': message}
    print(f"Sent Pushover alert '{ message }'!")
    return


def clear_alert(title, category):
    """
     Removes an alert from the state file, so that it may be sent again next time the same issue is detected.
     :param title: The title text for the alert.
     :param category: The category of the alert. (What health check function was alerting)
     """
    if 'active_alerts' in list(state):
        if category in list(state['active_alerts']):
            if title in list(state['active_alerts'][category]):
                print(f"Clearing alert '{ title }' in category '{ category }'.")
                del state['active_alerts'][category][title]


def zpool_list():
    """
    Executes 'zpool list -H' and parses the output.

    Please refer to the 'zpool' manual page for information on the various output fields.

    :return: List of pool dictionaries containing the data from 'zpool list -H'
    """
    # Run the base status command
    result = subprocess.run(['zpool', 'list', '-H'], stdout=subprocess.PIPE)

    # Collate data on pools
    zpools = []
    for zpool in result.stdout.strip().split(b'\n'):
        info = zpool.split(b'\t')
        pool = {'NAME': info[0].strip().decode(), 'SIZE': info[1].strip().decode(), 'ALLOC': info[2].strip().decode(),
                'FREE': info[3].strip().decode(), 'CKPOINT': info[4].strip().decode(),
                'EXPANDSZ': info[5].strip().decode(), 'FRAG': info[6].strip().decode(), 'CAP': info[7].strip().decode(),
                'DEDUP': info[8].strip().decode(), 'HEALTH': info[9].strip().decode(),
                'ALTROOT': info[10].strip().decode()}
        zpools.append(pool)
    
    # Return collected data
    return zpools


def check_pool_health():
    """
    Checks the health state of the ZFS Pools, and alerts if they are anything except 'ONLINE'

    Pools listed in either the '[global]/IgnorePools' or '[check_pool_health]/IgnorePools' settings will not alert.
    """
    # The category of our alert
    alert_category = 'check_pool_health'

    # The possible health states, an explanation of each, and their priority.
    pool_states = {
        'DEGRADED': {'explanation': "One or more disks are 'FAULTED', but enough disks remain for the pool to be "
                                    "operational.", 'priority': 1},
        'FAULTED': {'explanation': "One or more disks are 'FAULTED', but not enough disks remain for the pool to be "
                                   "operational.", 'priority': 2},
        'OFFLINE': {'explanation': "The pool was taken offline by the 'zpool offline' command.", 'priority': 0},
        'REMOVED': {'explanation': "The pool was removed from the server.", 'priority': 1},
        'UNAVAIL': {'explanation': "The pool is currently unavailable.", 'priority': 1}
    }

    # Iterate through all pools
    for pool in pools:
        # Get the current health state of the pool
        current_state = pool['HEALTH']
        # Iterate through all possible unhealthy states
        for possible_state in list(pool_states):
            title = f"{pool['NAME']} health is '{possible_state}'!"
            if current_state == possible_state:  # This pool is in this unhealthy state...
                priority = pool_states[current_state]['priority']
                message = f"The ZFS pool {pool['NAME']} on {socket.gethostname()} has the health state of " \
                          f"'{ current_state }'! { pool_states[current_state]['explanation'] }"
                if (pool['NAME'] not in conf['global']['IgnorePools'].split(','))\
                        and (pool['NAME'] not in conf[alert_category]['IgnorePools'].split(',')):
                    # The pool is unhealthy and is not in an ignore list, fire off an alert
                    send_alert(message, title, alert_category, priority=priority)
            else:
                # This pool is not currently in this unhealthy state, so clear any alert about this pool and state.
                clear_alert(title, alert_category)


def check_pool_capacity():
    """
    Checks the used capacity of the ZFS Pools, and alerts if they are over capacity.

    Pools listed in either the '[global]/IgnorePools' or '[check_pool_capacity]/IgnorePools' settings will not alert.
    """
    # The category of our alert
    alert_category = 'check_pool_capacity'

    # Iterate through all pools
    for pool in pools:
        # Determine the used capacity, and the titles of our various levels of alerts.
        capacity = int(pool['CAP'].strip('%'))
        normal_title = f"{ pool['NAME'] } over capacity"
        high_title = f"{ pool['NAME'] } over high capacity"
        emergency_title = f"{ pool['NAME'] } over emergency capacity"

        # Determine what alerts we should send and clear
        if capacity >= int(conf[alert_category]['EmergencyAlertThreshold']):  # Emergency alert
            priority = 2
            title = emergency_title
            message = f"The ZFS pool { pool['NAME'] } on { socket.gethostname() } has exceeded the emergency used  " \
                      f"capacity threshold of { conf[alert_category]['EmergencyAlertThreshold'] }%! "
        elif capacity >= int(conf[alert_category]['HighAlertThreshold']):  # High alert
            priority = 1
            title = high_title
            message = f"The ZFS pool { pool['NAME'] } on { socket.gethostname() } has exceeded the high used " \
                      f"capacity threshold of { conf[alert_category]['HighAlertThreshold'] }%! "
            clear_alert(emergency_title, alert_category)
        elif capacity >= int(conf[alert_category]['NormalAlertThreshold']):  # Normal alert
            priority = 0
            title = normal_title
            message = f"The ZFS pool { pool['NAME'] } on { socket.gethostname() } has exceeded the used capacity " \
                      f"threshold of { conf[alert_category]['NormalAlertThreshold'] }%!"
            clear_alert(emergency_title, alert_category)
            clear_alert(high_title, alert_category)
        else:  # Capacity is OK, we should clear all active alerts.
            clear_alert(emergency_title, alert_category)
            clear_alert(high_title, alert_category)
            clear_alert(normal_title, alert_category)
            continue

        # We have an alert to send for the pool, so we dispatch it unless it is in one of the ignore lists.
        if (pool['NAME'] not in conf['global']['IgnorePools'].split(','))\
                and (pool['NAME'] not in conf[alert_category]['IgnorePools'].split(',')):
            send_alert(message, title, alert_category, priority=priority)


if __name__ == "__main__":
    # File locations
    CONF_FILE = "/root/.config/zpool-alerts/settings.ini"  # Where to store configuration information
    STATE_FILE = "/root/.config/zpool-alerts/state.json"  # Where to store state information

    # Read in our configuration
    global conf
    conf = load_conf_file(CONF_FILE)

    # Read in the script's state information
    global state
    state = load_state_file(STATE_FILE)

    # Run our health checks and alert if issues found
    pools = zpool_list()
    check_pool_health()
    check_pool_capacity()
    # TODO: Write check_vdev_health() health function using 'zpool status' and 'glabel status'

    # Save the script's state information
    save_state_file(STATE_FILE)
